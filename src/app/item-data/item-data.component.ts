import { Component, OnInit,HostBinding } from '@angular/core';
import {articulos} from '../models/articulos.model'; // Importacion de la clase arituculos

@Component({
  selector: 'app-item-data',
  templateUrl: './item-data.component.html',
  styleUrls: ['./item-data.component.css']
})
export class ItemDataComponent implements OnInit {
   listaArticulos:articulos[]; // Lista Acumuladora
    total:number; //acumulador del Total General
    @HostBinding('attr.class') cssClass = 'fuente'; //Se Resaltan los textos, con la clase resaltar
  constructor() {
    //Se inicializan los atibutos
    this.listaArticulos=[]; 
    this.total=0;
   }
   limpiar(){

   }
  agregar(nombre:string,cantidad:string,precio:string):boolean{
    this.listaArticulos.push(new articulos(nombre,cantidad,precio));
    this.total+=(parseInt(precio)*parseInt(cantidad)); //Se acumula la sumatoria de los valores totales para optener el total general

    return false;
    
  }

  ngOnInit(): void {

  }
}
